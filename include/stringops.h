/* 
    File for simplifying some string operations
    stdio.h and stdlib.h must be included in main file

    getinput() - gets almost unlimited user`s input char by char
    rotatechar() - changes char according to given number (for Caesar`s and Gronsfeld`s ciphers)

*/
char* getinput(void);
char rotatechar(char ch, int num);

#define INIT_BUFFER_SIZE 20

char* getinput()
{
    char c;
    unsigned short int current_buffer_size = INIT_BUFFER_SIZE;
    unsigned short int index = 0;

    char* p = malloc(current_buffer_size * sizeof(char));
    if (p == NULL)
    {
        fprintf(stderr, "Malloc failed!");
        exit(1);
    }

    while(1)
    {
        c = getchar();
        if (index == current_buffer_size)
        {
            current_buffer_size *= 2;
            p = realloc(p, current_buffer_size * sizeof(char));
            if (p == NULL)
            {   
                fprintf(stderr, "Realloc failed !");
                exit(1);
            }
        }
        if (c == EOF || c == '\n')
        {
            p[index] = '\0';
            break;
        }
        p[index] = c;
        index++;
    }
    return p;
}

char rotatechar(char ch, int num)
{
    unsigned char rotated_ch;
    while (num < 0) num += 26; // % in C is not a modulo operator, but a remainder. 
                               // It cannot deal with negative numbers
    if (isalpha(ch))
    {
        rotated_ch = (int)ch + (num % 26);
        if (isupper(ch))
        {
            while (rotated_ch > 90) // 90 - 'Z' in ASCII 
            {
                rotated_ch -= 26;
            }

        }
        else if (islower(ch))
        {
            while (rotated_ch > 122) // 122 - 'z' in ASCII 
            {
                rotated_ch -= 26;
            }
        }
    }
    else
    {
        rotated_ch = ch;
    }
    return (char) rotated_ch;
}
