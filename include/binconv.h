/* 
    File for performing conversion from binary string
    to char and from char to binary string
    Dependencies: stdlib.h (for malloc)
*/
char *ch_to_bin(char ch);
char bin_to_ch(char *str);
unsigned long pow_of_2(unsigned short power);

char *ch_to_bin(char ch)
{
    char *str = malloc(8 * sizeof(char)); 
    unsigned short num = (unsigned short) ch;
    char i;
    for (i = 7; i >= 0; i--)
    {
        str[(int)i] = num % 2 == 0 ? '0' : '1';
        num /= 2;
    }
    return str;
}

char bin_to_ch(char *str)
{
    unsigned char ch = 0;
    char i;
    for (i = 7; i >= 0; i--)
    {
        ch += (str[i] - '0') * pow_of_2(7 - i);
    }
    return ch;
}

unsigned long pow_of_2(unsigned short power)
{
    unsigned int ret = 1;
    while(power > 0)
    {
        ret *= 2;
        power--;
    }
    return ret;

}
