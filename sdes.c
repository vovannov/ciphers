/*

    This is a Simplified DES cipher implementation, written in C
    Made by: Vladimir Pastukhov


    SDES encryption algorithm:

    0) input data is divided into 8-bit blocks


        Steps performed on each block:

    1) initial permutation (IP) of 8-bit data block is
        performed

    2) 4-bit R-part of the block is expanded to 8 bit by 
        expansion function (EP)

    3) 8-bit round key is calculated from initial 10-bit key

    4) exclusive OR (XOR) operation is performed on 
        expanded R-part (step 2) and round key (step 3)

    5) S-boxes are applied to result of step 4:
        S0 for left 4 bits and S1 for right 4 bits 

    6) permutation is applied to the result of step 5

    7) exclusive OR (XOR) is performed on L-part of initial 
        data and the result of step 6

    8) result of step 7 now becomes R-block for next round, 
        R-block of initial data becomes L-block for next round

    9) steps 2-7 are repeated with new values of L and R with second round key

    10) final permutation (FP) is applied to the result after step 9

    11) 8-bit block of ciphertext is formed after step 10. Then everything
        is repeated for each block of input data

    
    For decryption the same algorithm is used on ciphertext, 
    but on first round of decryption the second round key is used 
    and on second round the first round key is used. 
    Note: for decryption initial permutation (IP) is used first
    and final permutation (FP) is used last. Not in the reverse order

*/
    
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ENCRYPT 1 
#define DECRYPT 2
#define INPUT_BUFFER_SIZE 15
#define DATA_BLOCK_SIZE 8
#define KEY_SIZE 10
#define ROUND_KEY_SIZE 8
#define ROUNDS 2

// function prototypes
char *getinput(void);
int validate_input(char *str);
char **divide_into_blocks(char *str);
char *permutate(char* str, char mode);
char *expansion(char *str);
char *getroundkey(char *key, char round);
char *xor(char *str1, char *str2);
char **crypt(char **data, char *key, char mode);
void check_null_pointer(char *p);

// indexes for all operations from https://www.wou.edu/~beaverc/440/W15/7%20DES%20and%20SDES.pdf
int8_t k1_ind[] = { 0, 6, 8, 3, 7, 2, 9, 5 }; // 8-bit first round key from initital 10-bit key
int8_t k2_ind[] = { 7, 2, 5, 4, 9, 1, 8, 0 }; // 8-bit second round key from initial 10-bit key
int8_t IP[] =     { 1, 5, 2, 0, 3, 7, 4, 6 }; // initial permutation for data block
int8_t FP[] =     { 3, 0, 2, 4, 6, 1, 7, 5 }; // final permutation for data block
int8_t EP[] =     { 3, 0, 1, 2, 1, 2, 3, 0 }; // expansion function for R block
int8_t P4[] =     { 1, 3, 2, 0 };
// S-boxes, applied in function (R ^ ROUND_KEY) to transform 8-bit outcome to 4-bit 
char *S0[16][2] =  {               // S-box for left part
                {"0000", "01"}, 
                {"0001", "11"},
                {"0010", "00"},
                {"0011", "10"},
                {"0100", "11"},
                {"0101", "01"},
                {"0110", "10"},
                {"0111", "00"},
                {"1000", "00"},
                {"1001", "11"},
                {"1010", "10"},
                {"1011", "01"},
                {"1100", "01"},
                {"1101", "11"},
                {"1110", "11"},
                {"1111", "10"},
                };
char *S1[16][2] =  {               // S-box for right part
                {"0000", "00"},
                {"0001", "10"},
                {"0010", "01"},
                {"0011", "00"},
                {"0100", "10"},
                {"0101", "01"},
                {"0110", "11"},
                {"0111", "11"},
                {"1000", "11"},
                {"1001", "10"},
                {"1010", "00"},
                {"1011", "01"},
                {"1100", "01"},
                {"1101", "00"},
                {"1110", "00"},
                {"1111", "11"},
                };

uint32_t blocks; // total count of 8-bit blocks

int main()
{
    printf("Enter a binary string: ");
    char *inp_str = getinput();
    if (!validate_input(inp_str))
    {
        fprintf(stderr, "You didn`t enter a valid binary string!\n");
        exit(1);
    }

    printf("Enter a key: ");
    char *key = getinput();
    if (!validate_input(key) || strlen(key) != KEY_SIZE)
    {
        fprintf(stderr, "You didn`t enter a valid key!\n");
        exit(1);
    }

    char **data = divide_into_blocks(inp_str);
    free(inp_str);

    uint32_t i;

    printf("Initial data  : ");

    for (i = 0; i < blocks; i++)
    {
        printf("%s ", data[i]);
    }
    printf("\n");

    char **enc = crypt(data, key, ENCRYPT);

    printf("Encrypted data: ");

    for (i = 0; i < blocks; i++)
    {
        printf("%s ", enc[i]);
    }
    printf("\n");

    char **dec = crypt(enc, key, DECRYPT);
    printf("Decrypted data: ");

    for (i = 0; i < blocks; i++)
    {
        printf("%s ", dec[i]);
    }
    printf("\n");

    return 0;
}

char *getinput()
{
    char c;
    uint32_t current_buffer_size = INPUT_BUFFER_SIZE;
    uint32_t index = 0;

    char *p = malloc(current_buffer_size * sizeof(char));
    check_null_pointer(p);

    while(1)
    {
        c = getchar();
        if (index == current_buffer_size)
        {
            current_buffer_size *= 2;
            p = realloc(p, current_buffer_size * sizeof(char));
            check_null_pointer(p);
        }
        if (c == EOF || c == '\n')
        {
            p[index] = '\0';
            break;
        }
        if (c == ' ')
        {
            continue;
        }
        p[index] = c;
        index++;
    }
    return p;
}

int validate_input(char *str)
{
    while (*str)
    {
        if (*str != '0' && *str != '1')
            return 0;
        str++;
    }
    return 1;
}

char **divide_into_blocks(char *str)
{
    uint32_t str_len = strlen(str);
    blocks = str_len / DATA_BLOCK_SIZE;
    if (str_len % DATA_BLOCK_SIZE != 0)
    {
        blocks++;
    }
    char **data = malloc(blocks * sizeof(char*));
    if (data == NULL)
    {
        fprintf(stderr, "Malloc failed!\n");
        exit(1);
    }
    uint32_t counter = 0;
    uint8_t end = 0;
    for (uint32_t i = 0; i < blocks; i++)
    {
        data[i] = malloc(DATA_BLOCK_SIZE + 1); // 1 for '\0'
        for (uint32_t j = 0; j < DATA_BLOCK_SIZE; j++, counter++)
        {
            if (str[counter] == '\0')
            {
                end = 1;
            }
            if (end == 0)
            {
                data[i][j] = str[counter];
            }
            else
            {
                data[i][j] = '0';
            }
        }
        data[i][DATA_BLOCK_SIZE] = '\0';
    }
    return data;
}

char *permutate(char* str, char mode)
{
    char* ret = malloc(DATA_BLOCK_SIZE);
    check_null_pointer(ret);
    uint16_t i;
    switch (mode)
    {
        case 1: 
            for(i = 0; i < DATA_BLOCK_SIZE; i++)
            {
                ret[i] = str[IP[i]];
            }
            return ret;
        case 2:
            for(i = 0; i < DATA_BLOCK_SIZE; i++)
            {
                ret[i] = str[FP[i]];
            }
            return ret;
        default:
            fprintf(stderr, "Invalid mode in permutation()!\n");
            exit(2);
    }
}

char *expansion(char *str)
{
    char *ret = malloc(DATA_BLOCK_SIZE);
    check_null_pointer(ret);
    for (uint16_t i = 0; i < DATA_BLOCK_SIZE; i++)
    {
        ret[i] = str[EP[i]];
    }
    return ret;
}

char *getroundkey(char *key, char round)
{
    char *round_key = malloc(ROUND_KEY_SIZE); 
    check_null_pointer(round_key);

    uint16_t i;
    switch (round)
    {
        case 1:
            for(i = 0; i < ROUND_KEY_SIZE; i++)
            {
                round_key[i] = key[k1_ind[i]];
            }
            return round_key;       
        case 2:
            for(i = 0; i < ROUND_KEY_SIZE; i++)
            {
                round_key[i] = key[k2_ind[i]];
            }
            return round_key;       
        default:
            fprintf(stderr, "Invalid round number in getroundkey()!\n");
            exit(2);
    }
}

char *xor(char *str1, char *str2)
{
    char *ret = calloc(strlen(str1), sizeof(char));
    check_null_pointer(ret);
    for (uint32_t i = 0; i < strlen(str1); i++)
    {
        ret[i] = ((str1[i] - '0') ^ (str2[i] - '0')) + '0';
    }
    return ret;
}

char **crypt(char **data, char *key, char mode)
{
    char **ret = malloc(blocks * sizeof(char*));
    if (ret == NULL)
    {
        fprintf(stderr, "Malloc failed!\n");
        exit(1);
    }
    for(uint32_t i = 0; i < blocks; i++)
    {
        ret[i] = calloc(DATA_BLOCK_SIZE + 1, sizeof(char)); // 1 for '\0'
        check_null_pointer(ret[i]);

        memcpy(ret[i], data[i], DATA_BLOCK_SIZE);

        ret[i] = permutate(ret[i], 1); // initial permutation

        char *L = calloc(DATA_BLOCK_SIZE / 2, sizeof(char));
        check_null_pointer(L);

        memcpy(L, ret[i], DATA_BLOCK_SIZE / 2);

        char *R = calloc(DATA_BLOCK_SIZE / 2, sizeof(char));
        check_null_pointer(R);

        memcpy(R, ret[i] + 4, DATA_BLOCK_SIZE / 2);

        for (uint16_t round = 0; round < ROUNDS; round++)
        {
            char *expanded_R = expansion(R); // expansion function on R-block of data

            char *round_key;
            switch (mode)
            {
                case ENCRYPT:
                    round_key = getroundkey(key, round + 1);
                    break;
                case DECRYPT:
                    round_key = getroundkey(key, ROUNDS - round);
                    break;
                default:
                    fprintf(stderr, "Invalid mode %c!\n", mode);
                    exit(3);
            }
            char *xored = xor(expanded_R, round_key); // xor of expanded R-block with round key
            free(round_key);

            // Split the result of (R XOR round_key) for S-blocks
            char *xored_L = malloc(DATA_BLOCK_SIZE / 2);
            check_null_pointer(xored_L);

            memcpy(xored_L, xored, DATA_BLOCK_SIZE / 2);
            
            char *xored_R = malloc(DATA_BLOCK_SIZE / 2);
            check_null_pointer(xored_R);

            memcpy(xored_R, xored + 4, DATA_BLOCK_SIZE / 2);

            free(xored);
            
            char *after_s_box = calloc(DATA_BLOCK_SIZE / 2, sizeof(char));
            check_null_pointer(after_s_box);
            
            // S-blocks
            uint16_t s_index;
            for (s_index = 0; s_index < 16; s_index++)
            {
                if (strcmp(xored_L, S0[s_index][0]) == 0)
                {
                    memcpy(after_s_box, S0[s_index][1], 2);
                    break;
                }
            }
            for (s_index = 0; s_index < 16; s_index++)
            {
                if (strcmp(xored_R, S1[s_index][0]) == 0)
                {
                    memcpy(after_s_box + 2, S1[s_index][1], 2);
                    break;
                }
            }
            // S-blocks end
            free(xored_L);
            free(xored_R);

            // P4  
            char *after_p4 = calloc(DATA_BLOCK_SIZE / 2, sizeof(char));
            check_null_pointer(after_p4);

            for (uint8_t p4_index = 0; p4_index < 4; p4_index++)
            {
                after_p4[p4_index] = after_s_box[P4[p4_index]];
            }
            free(after_s_box);

            // L-part xor with things after function and swap of L and R
            if (round == 0)
            {
                char *temp = R;
                R = xor(after_p4, L);
                L = temp;
            }
            // final round without swap
            else
            {
                L = xor(after_p4, L);
            }
            free(after_p4);
        }
        memcpy(ret[i], L, DATA_BLOCK_SIZE / 2);
        memcpy(ret[i] + 4, R, DATA_BLOCK_SIZE / 2);

        ret[i] = permutate(ret[i], 2); // final permutation
        ret[i][DATA_BLOCK_SIZE] = '\0';
    }
    return ret;
}

void check_null_pointer(char *p)
{
    if (p == NULL)
    {
        fprintf(stderr, "Malloc failed, got NULL pointer\n");
        exit(1);
    }
}
