#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/stringops.h"

#define ENC 1
#define DEC 2

int32_t GCD(int32_t a, int32_t b);
int32_t index_of(char ch);
char crypt_char(char ch, int32_t k1, int32_t k2, char mode);
char *crypt(char *str, int32_t k1, int32_t k2, char mode);
void calc_mul_inverse(int32_t num, int32_t mod);

const char *alphabet = "abcdefghijklmnopqrstuvwxyz";

const int8_t mul_inv_nums[12][2] = {{1,  1},  // precomputed multiplicative inverse numbers
                                    {3,  9},
                                    {5,  21},
                                    {7,  15},
                                    {9,  3},
                                    {11, 19},
                                    {15, 7},
                                    {17, 23},
                                    {19, 11},
                                    {21, 5},
                                    {23, 17},
                                    {25, 25}};

int32_t mul_inv_num;

int main()
{
    printf("Enter a string to encrypt: ");
    char *to_enc = getinput();

    printf("Enter first key: ");
    int32_t k1  = 1;
    scanf("%d", &k1);

    printf("Enter second key: ");
    int32_t k2  = 1;
    scanf("%d", &k2);

    char *enc = crypt(to_enc, k1, k2, ENC);
    char *dec = crypt(enc, k1, k2, DEC);
    printf("Initial string: %s\n", to_enc);
    printf("Encrypted: %s\n", enc);
    printf("Decrypted: %s\n", dec);
    return 0;
}

int32_t GCD(int32_t a, int32_t b) // Greatest common divisor, calculated by Euclidus algorithm
{
    while (a > 0 && b > 0)
    {
        if (a > b) a %= b;
        else if (b > a) b %= a;
    }

    return a + b;
}

/* ASCII code of character to index in the alphabet */
/* e.g. a(97)  --> a(0) */
int32_t index_of(char ch)
{
    int index = 0;
    if (isupper(ch))
        index = (int)ch - 'A';

    else if (islower(ch))
        index = (int)ch - 'a';

    return index;
}

char crypt_char(char ch, int32_t k1, int32_t k2, char mode)
{
    char upper = isupper(ch) ? 1 : 0 ;
    char ret_ch;
    int index = 0;
    if (mode == ENC)
    {
        index = (index_of(ch) * k1) % 26;
        index += k2;
        index = index % 26;
    }
    else if (mode == DEC)
    {
        index = index_of(ch) - k2;
        if (index < 0) index += 26;
        index *= mul_inv_num;
        index = index % 26;
    }
    ret_ch = alphabet[index];
    if (upper) ret_ch &= 223;
    return ret_ch;
}

char *crypt(char *str, int32_t k1, int32_t k2, char mode)
{
    char *ret = malloc(strlen(str));
    if (mode == DEC) calc_mul_inverse(k1, 26); // calculate multipl inverse only once
    for(uint32_t i = 0; i < strlen(str); i++)
    {
        if (isalpha(str[i]))
            ret[i] = crypt_char(str[i], k1, k2 % 26, mode);
        else
            ret[i] = str[i];
    }
    return ret;
}

void calc_mul_inverse(int32_t num, int32_t mod)
{
    if (GCD(num, mod) != 1)
    {
        fprintf(stderr, "Multiplicative inverse for number %d modulo %d does not exist!\n", num, mod);
        exit(1);
    }
    num %= mod;
    for (int8_t i = 0; i < 12; i++)
    {
        if (mul_inv_nums[i][0] == num)
        {
            mul_inv_num = mul_inv_nums[i][1];
            return;
        }
    }
}
