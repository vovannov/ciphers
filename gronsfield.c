#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "include/stringops.h"

char* crypt(char* text, char* key, short mode);

int main()
{
    char* text;
    char* key;
    char* cr;
    char mode; 

    printf("Enter a phrase to encrypt or decrypt: ");
    text = getinput();
    printf("Enter a key: ");
    key = getinput();
    if (strlen(key) < 2)
    {
        fprintf(stderr, "You didn`t enter a key! Exiting...\n");
        exit(1);
    }

    printf("Choose mode: \n \t1) Encrypt\n\t2) Decrypt\nYour choice: ");
    mode = getchar();

    if (mode != '1' && mode != '2')
    {
        printf("You`ve entered some invalid garbage, selecting encrypt mode\n");
        mode = '1';
    }
    if (mode == '1')
    {
        cr = crypt(text, key, 1);

        printf("Initial text: %s\n", text);
        printf("Encrypted text: %s\n", cr);
    }
    else if (mode == '2')
    {
        cr = crypt(text, key, 2);

        printf("Initial text: %s\n", text);
        printf("Decrypted text: %s\n", cr);

    }

    return 0; 
}

char* crypt(char* text, char* key, short mode)
{
    char* cr = malloc(sizeof(char) * strlen(text));
    if (!cr)
    {
        fprintf(stderr, "Memory allocation error!");
        exit(1);
    }
    
    uint16_t i, keyindex; 

    if (mode == 1)
    {
        for (i = 0, keyindex = 0; i < strlen(text); i++)
        {
            if (text[i] == ' ')
            {
                cr[i] = ' ';
                continue;
            }
            cr[i] = rotatechar(text[i], ((uint16_t)key[keyindex % (strlen(key))] - '0'));
            keyindex++;
        }
    }
    else
    {
        for (i = 0, keyindex = 0; i < strlen(text); i++)
        {
            if (text[i] == ' ')
            {
                cr[i] = ' ';
                continue;
            }
            cr[i] = rotatechar(text[i], 26 - ((uint16_t)key[keyindex % (strlen(key))] - '0'));
            keyindex++;
        }
    }
    return cr;
}

