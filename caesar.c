#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "include/stringops.h"

char* rotate(char* str, short num);

int main()
{
    int number;
    char* str;

    printf("Enter a string: ");
    str = getinput();

    printf("Enter a key: "); 
    scanf("%d", &number);
    if (number == 0)
    {
        fprintf(stderr, "\nYou've entered an invalid key or 0\n");
        fprintf(stderr, "That is not fun. Selecting 13 as a rotation number\n\n");
        number = 13;
    }

    printf("Plain text string is: %s\n", str);
    char* enc = rotate(str, number);

    printf("Encrypted string: %s\n", enc);
    return 0;
}

char* rotate(char* str, short num)
{
    char* retstr = malloc(sizeof(char) * strlen(str));
    if (!retstr){
        fprintf(stderr, "Error in memory allocation\n");
        return NULL;
    }
    num = num % 26;
    for (uint16_t i = 0; i < strlen(str); i++)
    {
        retstr[i] = rotatechar(str[i], num);
    }
    return retstr;
}

