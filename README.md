# Ciphers in C
Implementations of some ciphers written in C.  
Currently available:  
* Cesar's cipher  
* Gronsfeld's cipher  
* Athen cipher  
* Simplified DES
